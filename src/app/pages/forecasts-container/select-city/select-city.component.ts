import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup, Validators, FormBuilder, PatternValidator} from '@angular/forms';
import {animate, style, transition, trigger} from '@angular/animations';
import {ForecastsService} from '../../../services/forecasts.service';

@Component({
  selector: 'app-select-city',
  templateUrl: './select-city.component.html',
  styleUrls: ['./select-city.component.scss'],
  providers: [ForecastsService],
  animations: [
    trigger(
      'errors',
      [
        transition(
          ':enter', [
            style({transition: 'ease-in', opacity: 0}),
            animate('1s', style({transition: 'ease-in', 'opacity': 1}))
          ]
        ),
        transition(
          ':leave', [
            style({transition: 'ease-out', opacity: 1}),
            animate('1s', style({transition: 'ease-out', 'opacity': 0}))

          ]
        )]
    )
  ]
})

export class SelectCityComponent implements OnInit {
  @Input() city: string;
  @Output() forecastsData: EventEmitter<string> = new EventEmitter();

  forecasts: FormGroup;

  constructor(
    private fb: FormBuilder,
    private forecastService: ForecastsService
  ) {}

  showCurrentForecasts() {
    this.forecastsData.emit('Senpai notice me!');
    this.forecastService.getCurrentForecast(this.forecasts.get('city').value);
  }


  ngOnInit() {
    this.forecasts = this.fb.group({
      city: ['Milano', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      // countryCode: ['IT', [Validators.required, Validators.pattern('^[A-Z0-9]{3}(?:List)?$')]]
    });
  }

}
