import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoForecastsComponent } from './info-forecasts.component';

describe('InfoForecastsComponent', () => {
  let component: InfoForecastsComponent;
  let fixture: ComponentFixture<InfoForecastsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoForecastsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoForecastsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
