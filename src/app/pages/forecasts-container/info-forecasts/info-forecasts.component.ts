import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-info-forecasts',
  templateUrl: './info-forecasts.component.html',
  styleUrls: ['./info-forecasts.component.scss']
})
export class InfoForecastsComponent implements OnInit {
  @Input() senpaiFeedsMe: string;
  constructor() { }

  ngOnInit() {
    console.log(this.senpaiFeedsMe);
  }

}
