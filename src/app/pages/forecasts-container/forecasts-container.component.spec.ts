import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastsContainerComponent } from './forecasts-container.component';

describe('ForecastsContainerComponent', () => {
  let component: ForecastsContainerComponent;
  let fixture: ComponentFixture<ForecastsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
