import { Component, OnInit } from '@angular/core';
import {ForecastsService} from '../../services/forecasts.service';

@Component({
  selector: 'app-forecasts-container',
  templateUrl: './forecasts-container.component.html',
  styleUrls: ['./forecasts-container.component.scss'],
  providers: [ForecastsService]
})
export class ForecastsContainerComponent implements OnInit {

  feedTheChan = 'Eat, my son!';

  constructor() { }

  //onForecastsReceived(time: string, tMax: string, tMin: string) {
    //console.log(time, tMax, tMin);
  //}

  noticeChan(messageFromChan: string) {
    console.log(messageFromChan);
  }

  ngOnInit() {
  }

}
