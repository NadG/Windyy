import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';

import { AppComponent } from './app.component';
import { ForecastsContainerComponent } from './pages/forecasts-container/forecasts-container.component';
import { SelectCityComponent } from './pages/forecasts-container/select-city/select-city.component';
import { InfoForecastsComponent } from './pages/forecasts-container/info-forecasts/info-forecasts.component';
import {ManipulateTemperatureService} from './services/manipulate-temperature.service';
import {ManipulateDateTimeService} from './services/manipulate-date-time.service';
import {ForecastsService} from './services/forecasts.service';

@NgModule({
  declarations: [
    AppComponent,
    SelectCityComponent,
    ForecastsContainerComponent,
    InfoForecastsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [ManipulateTemperatureService, ManipulateDateTimeService, ForecastsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
