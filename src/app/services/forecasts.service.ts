import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { ManipulateDateTimeService} from './manipulate-date-time.service';
import {ManipulateTemperatureService} from './manipulate-temperature.service';

@Injectable()

export class ForecastsService {
  // private THREE_HOURS_FORECASTS = `https://api.openweathermap.org/data/2.5/forecast?id=${this.CITY}&APPID=${this.API_KEY}`;
  // private DAILY_FORECASTS = `https://api.openweathermap.org/data/2.5/forecast/daily?id=${this.CITY}&APPID=${this.API_KEY}`;
  // https://api.openweathermap.org/data/2.5/forecast?q=milano,it&APPID=2ccba7ff46032a91be509494b65216a0
  // private CURRENT_FORECASTS = `https://api.openweathermap.org/data/2.5/weather?id=${this.CITY}&APPID=${this.API_KEY}`;

  private API_KEY = '2ccba7ff46032a91be509494b65216a0';
  private CITY = 'Milano';
  private URL = 'https://api.openweathermap.org/data/2.5/';
  private CURRENT_FORECAST = 'weather';
  private THREE_HOURS_FORECASTS = 'forecast';
  private DAILY_FORECASTS = 'forecast/daily';
  private headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});

  constructor(
    private http: HttpClient,
    private dateTimes: ManipulateDateTimeService,
    private tempConvertion: ManipulateTemperatureService
  ) {
    this.dateTimes = dateTimes;
    this.tempConvertion = tempConvertion;
    console.log('this.constructor.name ' + this.constructor.name);
  }

  getCurrentForecast(city) {
    return this.http.get(`${this.URL}${this.CURRENT_FORECAST}?q=${city}&APPID=${this.API_KEY}`)
      .subscribe(data => {
        console.log(data);
        let dateTime = data.dt;
        let minT = this.tempConvertion.kelvinToCelsius(data.main.temp_min).toString().slice(0, 2);
        let maxT = this.tempConvertion.kelvinToCelsius(data.main.temp_max).toString().slice(0, 2);
        let medT = this.tempConvertion.kelvinToCelsius(data.main.temp).toString().slice(0, 2);
        this.dateTimes.getDateTime(dateTime);
        console.log(minT, maxT, medT);
      });
  }
  // Forecasts for next 5 days every 3 hours
  /*getThreeHourForecasts(city) {
    return this.http.get(`https://api.openweathermap.org/data/2.5/forecast?q=${city},${countryCode}&APPID=${this.API_KEY}`)
    //return this.http.get(`${this.URL}${this.THREE_HOURS_FORECASTS}?q=${city}&APPID=${this.API_KEY}`)
      .subscribe(data => {
        console.log(data);
      });
  }
  getDailyForecasts(city) {
    return this.http.get(`${this.URL}${this.DAILY_FORECASTS}?q=${city}&APPID=${this.API_KEY}`);
  }*/
}
