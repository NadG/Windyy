import { Injectable } from '@angular/core';

@Injectable()
export class ManipulateTemperatureService {

  constructor() { }

  kelvinToCelsius(kelvin) {
    return kelvin - 273.15;
  }

}
