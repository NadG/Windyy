import { Injectable } from '@angular/core';

@Injectable()
export class ManipulateDateTimeService {

  constructor() { }

  getDateTime(dateTime) {
    let dt = new Date(dateTime * 1000).toLocaleString();
    console.log(dt);
    this.getHour(dt);
    this.getTime(dt);
  }

  getTime(dateTime) {
    let time = dateTime.slice(11, 16);
    console.log(time);
    return time;
  }

  getHour(dateTime) {
    let hour = dateTime.slice(0, 9);
    console.log(hour);
    return hour;
  }

}
