import { Component, Input } from '@angular/core';
import {ForecastsService} from './services/forecasts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ForecastsService]
})

export class AppComponent {
  constructor() {}

}
