import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ForecastsContainerComponent} from './pages/forecasts-container/forecasts-container.component';
import {SelectCityComponent} from './pages/forecasts-container/select-city/select-city.component';

const routes: Routes = [
  { path: 'home',
    component: ForecastsContainerComponent,
    children: [
      { path: '', redirectTo: '/select-city', pathMatch: 'full' },
      {path: 'select-city', component: SelectCityComponent},
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
