## Windy
Weather forecasts app based on OpenWeatherMap API and developed in Angular2+

### Flow

**AppComponent**
  - **SelectCityComponent**
    - when the city is provided and the server responds 200 the app will navigate to 
    - **ForecastsContainerComponent** : which will contain onInit:
      - **CurrentForecastsComponent** : as default
      - **DailyForecastsComponent** : displayed optionally
      - **ThreeHourForecastsComponent** : displayed optionally 
